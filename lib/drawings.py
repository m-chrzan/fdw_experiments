import constants
import plotting
import analysis as an

def show_experiment(pg, plt, variable, with_categories, ignore=['csv']):
    data = an.get_experiment_times(pg, variable, with_categories)
    for method in ignore:
        del data[method]
    normalized = an.normalize(data)
    plt.figure(figsize=(30, 12))
    plt.subplot(1, 2, 1)
    plotting.make_bars(plt, constants.experiments[variable], normalized)
    plotting.plot_defaults(plt, 'Czas wykonania przy zmiennym {}'.format(variable), 'Liczba {}'.format(variable), 'Czas wykonania\n(znormalizowany, normal = 1)')
    plt.subplot(1, 2, 2)
    plotting.make_lines(plt, constants.experiments[variable], data)
    plotting.plot_defaults(plt, 'Czas wykonania przy zmiennym {}'.format(variable), 'Liczba {}'.format(variable), 'Czas wykonania\n(sekundy)')

    plt.suptitle(
            'Czas wykonania przy zmiennym {}'.format(variable) +
            (',\nsuma z konkretnej kategorii' if with_categories else '')
    )
    plt.show()

def show_sizes(pg, plt, variable):
    data = an.get_experiment_sizes(pg, variable)
    normalized = an.normalize(data, 'csv')
    plt.figure(figsize=(20, 10))
    labels = list(map(lambda byts: '{:.2f}MB'.format(byts/(1000*1000)), data['csv']))
    plotting.make_bars(plt, labels, normalized)
    plotting.plot_defaults(
        plt,
        'Względny rozmiar na dysku przy zmiennym {}'.format(variable),
        'Rozmiar pliku CSV (bajty)'.format(variable),
        'Rozmiar na dysku\n(znormalizowany, csv = 1)'
    )
    plt.title('Względny rozmiar na dysku przy zmiennym {}'.format(variable))
    plt.show()
