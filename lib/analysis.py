import constants as constants

def get_experiment_times(pg, variable, with_category):
    template = """
        SELECT p.{variable}, t.method, avg(t.time)
        FROM experiment_params p, experiment_time t
        WHERE p.exp_id = t.exp_id AND
            {default1} AND {default2} AND {with_category}
        GROUP BY p.exp_id, t.method;
    """

    defaults = [
            '{} = {}'.format(var, constants.defaults[var])
            for var in list(constants.defaults)
            if var != variable
    ]

    statement = template.format(variable=variable, default1=defaults[0],
            default2=defaults[1], with_category='{}with_category'.format('' if
                with_category else 'NOT '))
    pg.execute(statement)

    fetched = pg.fetchall()
    data = { method: [0 for _ in constants.experiments[variable]] for method in constants.methods }
    for row in fetched:
        index = constants.experiments[variable].index(row[0])
        data[row[1]][index] = row[2]

    return data

def get_experiment_sizes(pg, variable):
    template = """
        SELECT p.{variable}, s.method, s.size
        FROM experiment_params p, experiment_size s
        WHERE p.exp_id = s.exp_id AND
            {default1} AND {default2}
    """

    defaults = [
            '{} = {}'.format(var, constants.defaults[var])
            for var in list(constants.defaults)
            if var != variable
    ]

    statement = template.format(variable=variable, default1=defaults[0],
            default2=defaults[1])
    pg.execute(statement)

    fetched = pg.fetchall()
    data = { method: [0 for _ in constants.experiments[variable]] for method in constants.methods }
    for row in fetched:
        index = constants.experiments[variable].index(row[0])
        data[row[1]][index] = row[2]

    for method in constants.methods:
        if method[-5:] == 'index':
            data[method] = [original + normal for (original, normal) in
                    zip(data[method], data['normal'])]

    return data

def get_all_experiment_sizes(pg):
    statement = """
        SELECT p.exp_id, s.method, s.size
        FROM experiment_params p, experiment_size s
        WHERE p.exp_id = s.exp_id AND p.query_days = 365 AND p.values = 50;
    """

    pg.execute(statement)

    fetched = pg.fetchall()

    csv_experiments = []
    for row in fetched:
        if row[1] == 'csv':
            csv_experiments.append((row[2], row[0]))

    csv_experiments.sort()
    sorted_exp_ids = list(map(lambda pair: pair[1], csv_experiments))

    data = { method: [0 for _ in sorted_exp_ids] for method in constants.methods }
    for row in fetched:
        index = sorted_exp_ids.index(row[0])
        data[row[1]][index] = row[2]

    for method in constants.methods:
        if method[-5:] == 'index':
            data[method] = [original + normal for (original, normal) in
                    zip(data[method], data['normal'])]

    return data

def normalize(data, normalize_to = constants.NORMAL):
    normalizers = data[normalize_to]
    normalized = {}
    for method in data:
        normalized[method] = [original/norm for (original, norm) in
                zip(data[method], normalizers)]
    return normalized
