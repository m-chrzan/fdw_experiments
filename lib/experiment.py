import constants
import datetime
from pathlib import Path
import os

from stats import Stats
import utils

class Experiment:
    """ Contains the logic for running an experiment """

    def __init__(self, params, postgres):
        self.stats = Stats()
        self.params = params
        self.postgres = postgres

    def generate_csv(self):
        self.generate_csv_file()
        self.generate_csv_table()
        self.set_csv_size()

    def generate_csv_file(self):
        generator_arguments = '{} {} {} --filename {}'.format(
            self.params.categories, self.params.values, self.params.days_total,
            'gen/experiment.csv'
        )
        os.system(
            'python scripts/generate_csv.py {}'.format(generator_arguments)
        )

    def generate_csv_table(self):
        self.generate_foreign_table(constants.CSV, constants.CSV_SERVER, { 
                'filename': '/home/m/zad/2/fdw_experiments/gen/experiment.csv',
                'format': 'csv',
                'header': 'true'
            }
        )

    def set_csv_size(self):
        self.stats.set_size(constants.CSV, self.find_csv_size())

    def find_csv_size(self):
        return os.path.getsize('/home/m/zad/2/fdw_experiments/gen/experiment.csv')

    def run_csv(self):
        self.do_experiment(constants.CSV)

    def do_experiment(self, suffix, method=None):
        if method is None:
            method = suffix

        for i in range(5):
            result = self.run(suffix)
            self.stats.add_run(method, utils.parse_time(result))

        for i in range(5):
            result = self.run(suffix, category=True)
            self.stats.add_category_run(method, utils.parse_time(result))

    def run(self, suffix, category=False):
        query = self.build_category_query(suffix) if category else self.build_query(suffix)
        statement = 'EXPLAIN ANALYZE ' + query + ';'

        self.postgres.execute(statement)
        return self.postgres.fetchall()

    def build_query(self, suffix):
        template = """
            SELECT SUM(v{}) FROM experiment_{} WHERE date BETWEEN '{}' AND '{}'
        """

        date_from = datetime.date(2015, 1, 1) + datetime.timedelta(
                days=self.params.days
        )
        date_to = date_from + datetime.timedelta(days=self.params.days)

        value = self.params.values // 2

        return template.format(value, suffix, date_from, date_to)

    def build_category_query(self, suffix):
        template = self.build_query(suffix) + " AND category = 'c{}'"

        category = self.params.categories // 2

        return template.format(category)

    def generate_table(self):
        self.create_table()
        self.copy_to_table(constants.NORMAL)
        self.store_table_size()

    def store_table_size(self):
        oid = self.get_oid(constants.NORMAL)
        size = self.get_base_size(oid)
        self.stats.set_size(constants.NORMAL, size)

    def get_oid(self, suffix):
        statement = """
            SELECT oid FROM pg_class WHERE relname = 'experiment_{}'
        """.format(suffix)

        self.postgres.execute(statement) 
        return self.postgres.fetchone()[0]

    def get_base_size(self, oid):
        return self.get_size('base', oid)

    def get_cstore_size(self, oid):
        return self.get_size('cstore_fdw', oid)

    def get_size(self, directory, oid):
        directory = Path(constants.PG_DIR, directory, str(constants.DB_OID))
        files = directory.glob('{}*'.format(oid))
        size = 0
        for f in files:
            size += os.path.getsize(f)

        return size

    def create_table(self):
        statement = """
            CREATE TABLE experiment_normal (
                {}
            );
        """.format(self.generate_columns_string())

        self.postgres.execute(statement)

    def copy_to_table(self, suffix):
        statement = """
            COPY experiment_{} FROM '/home/m/zad/2/fdw_experiments/gen/experiment.csv' WITH csv HEADER
        """.format(suffix)
        self.postgres.execute(statement)

    def run_table(self):
        self.do_experiment(constants.NORMAL)

    def generate_date_index(self):
        statement = """
            CREATE INDEX experiment_date_index ON experiment_normal (date);
        """

        self.postgres.execute(statement)
        self.store_date_index_size()

    def store_date_index_size(self):
        oid = self.get_oid(constants.DATE_INDEX)
        size = self.get_base_size(oid)
        self.stats.set_size(constants.DATE_INDEX, size)

    def run_with_date_index(self):
        self.do_experiment(constants.NORMAL, method=constants.DATE_INDEX)

    def remove_date_index(self):
        statement = """
            DROP INDEX experiment_date_index;
        """

        self.postgres.execute(statement)

    def analyze(self):
        self.postgres.execute('ANALYZE;')

    def generate_category_index(self):
        statement = """
            CREATE INDEX experiment_category_index ON experiment_normal (category);
        """

        self.postgres.execute(statement)
        self.store_category_index_size()

    def store_category_index_size(self):
        oid = self.get_oid(constants.CATEGORY_INDEX)
        size = self.get_base_size(oid)
        self.stats.set_size(constants.CATEGORY_INDEX, size)

    def run_with_category_index(self):
        self.do_experiment(constants.NORMAL, method=constants.CATEGORY_INDEX)

    def generate_cstore_table(self):
        self.generate_foreign_table(constants.CSTORE, constants.CSTORE_SERVER, {})
        self.copy_to_table(constants.CSTORE)
        self.store_cstore_size()

    def store_cstore_size(self):
        oid = self.get_oid(constants.CSTORE)
        size = self.get_cstore_size(oid)
        self.stats.set_size(constants.CSTORE, size)

    def run_cstore(self):
        self.do_experiment(constants.CSTORE)

    def cleanup(self):
        self.remove_table(constants.CSV, foreign = True)
        self.remove_table(constants.NORMAL)
        self.remove_table(constants.CSTORE, foreign = True)

    def remove_table(self, suffix, foreign = False):
        statement = 'DROP {} TABLE experiment_{}'.format('FOREIGN' if foreign
                else '', suffix)
        self.postgres.execute(statement)

    def generate_foreign_table(self, suffix, server, options):
        template = """
            CREATE FOREIGN TABLE experiment_{} (
                {}
            ) SERVER {} {};
        """

        columns_string = self.generate_columns_string()

        options_string = ''
        if options:
            bare_options = ["{} '{}'".format(option, options[option]) for option
                    in list(options)]
            options_string = 'OPTIONS ( {} )'.format(', '.join(bare_options))

        statement = template.format(suffix, columns_string, server,
                options_string)

        self.postgres.execute(statement)

    def generate_columns_string(self):
        value_columns = ['{} INT'.format(column) for column in
                self.params.get_value_columns()]
        value_columns_string = ', '.join(value_columns)
        return 'date DATE, category VARCHAR, ' + value_columns_string
