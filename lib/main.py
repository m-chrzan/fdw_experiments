import psycopg2

import constants
from experiment import Experiment
from params import Params

test_params = []
for experiment_type in constants.experiment_types:
    for value in constants.experiments[experiment_type]:
        p = Params(**{ arg: value if arg == experiment_type else constants.defaults[arg]
            for arg in constants.experiment_types})
        test_params.append(p)

def run_experiment(params, pg, ident):
    e = Experiment(params, pg)

    e.cleanup()
    e.generate_csv()
    e.generate_table()
    e.generate_cstore_table()

    e.run_csv()
    e.analyze()
    e.run_table()
    e.generate_date_index()
    e.run_with_date_index()
    e.remove_date_index()
    e.generate_category_index()
    e.run_with_category_index()
    e.run_cstore()

    csv_stats = e.stats.get_stats(constants.CSV)
    normal_stats = e.stats.get_stats(constants.NORMAL)
    date_index_stats = e.stats.get_stats(constants.DATE_INDEX)
    category_index_stats = e.stats.get_stats(constants.CATEGORY_INDEX)
    cstore_stats = e.stats.get_stats(constants.CSTORE)

    print(csv_stats)
    print(normal_stats)
    print(date_index_stats)
    print(category_index_stats)
    print(cstore_stats)
    print()

    params.save_to_db(pg, ident)
    e.stats.save_to_db(pg, ident)

with psycopg2.connect("dbname=m user=m") as conn, conn.cursor() as pg:

    ident = 0
    for params in test_params:
        run_experiment(params, pg, ident)
        ident += 1
