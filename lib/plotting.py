def make_bars(plt, x_labels, data):
    """
    data is a dict with: {
        'method': [value for each x_label]
    }
    """
    total_width = 0.9
    width_per_bar = total_width / len(data)
    current_offset = - (width_per_bar * (len(data) / 2)) + width_per_bar/2
    for series in list(data):
        xs = [i + current_offset for i in range(len(x_labels))]
        ys = data[series]
        plt.bar(xs, ys, width = width_per_bar, label=series)
        current_offset += width_per_bar

    plt.xticks(range(len(x_labels)), x_labels)

def make_lines(plt, xs, data):
    for method in data:
        plt.plot(xs, data[method], label=method)

def plot_defaults(plt, title, xlabel, ylabel):
    plt.legend()
    # plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
