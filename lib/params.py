class Params:
    def __init__(self, categories, values, days):
        self.categories = categories
        self.values = values
        self.days = days
        self.days_total = 3 * days
        self.columns = ['date', 'category'] + [
                'v{}'.format(i) for i in range(values)
        ]

    def get_value_columns(self):
        return self.columns[2:]

    def save_to_db(self, postgres, ident):
        statement = """
            INSERT INTO experiment_params VALUES ({}, {}, {}, {}, {});
        """.format(
            ident, self.categories, self.values, self.days, self.days_total
        )

        postgres.execute(statement)

    def __str__(self):
        return "Categories: {} Values: {} Days: {} Total days: {}".format(
                self.categories, self.values, self.days, self.days_total)
