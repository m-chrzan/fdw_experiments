CSV_SERVER = 'csvs'
CSTORE_SERVER = 'cstore_server'

CSV = 'csv'
NORMAL = 'normal'
DATE_INDEX = 'date_index'
CATEGORY_INDEX = 'category_index'
CSTORE = 'cstore'

methods = [CSV, NORMAL, DATE_INDEX, CATEGORY_INDEX, CSTORE]

DB_OID = 16385
PG_DIR = '/var/lib/postgresql/12/main/'

experiment_types = ['categories', 'values', 'query_days']

defaults = {
        'categories': 20,
        'values': 50,
        'query_days': 365
}

experiments = {
        'categories': [1, 10, 20, 100, 500],
        'values': [1, 10, 50, 100, 500, 1000],
        'query_days': [1, 7, 30, 365, 365 * 2]
}
