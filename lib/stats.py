import constants

class MethodStats:
    def __init__(self, name):
        self.name = name
        self.runs = []
        self.category_runs = []

        self.size = 0

    def add_run(self, time):
        self.runs.append(time)

    def add_category_run(self, time):
        self.category_runs.append(time)

    def set_size(self, size):
        self.size = size

    def save_to_db(self, postgres, ident):
        self.save_times(postgres, ident)
        self.save_size(postgres, ident)

    def save_times(self, postgres, ident):
        template = """
            INSERT INTO experiment_time VALUES ({}, {}, '{}', {});
        """

        for run in self.runs:
            statement = template.format(ident, run, self.name, 'false')
            postgres.execute(statement)

        for run in self.category_runs:
            statement = template.format(ident, run, self.name, 'true')
            postgres.execute(statement)

    def save_size(self, postgres, ident):
        statement = """
            INSERT INTO experiment_size VALUES ({}, {}, '{}');
        """.format(ident, self.size, self.name)

        postgres.execute(statement)

    def __str__(self):
        return '\n'.join([
                self.name + ':',
                'Runs: ' + str(self.runs),
                'Category runs: ' + str(self.category_runs),
                'Size: ' + str(self.size)
        ])



class Stats:
    """ Stores statistics from an experiment """

    def __init__(self):
        self.stats = {
                constants.CSV: MethodStats(constants.CSV),
                constants.NORMAL: MethodStats(constants.NORMAL),
                constants.DATE_INDEX: MethodStats(constants.DATE_INDEX),
                constants.CATEGORY_INDEX: MethodStats(constants.CATEGORY_INDEX),
                constants.CSTORE: MethodStats(constants.CSTORE)
        }

    def add_run(self, name, time):
        self.stats[name].add_run(time)

    def add_category_run(self, name, time):
        self.stats[name].add_category_run(time)

    def set_size(self, name, size):
        self.stats[name].set_size(size)

    def get_stats(self, name):
        return self.stats[name]

    def save_to_db(self, postgres, ident):
        for name in list(self.stats):
            self.stats[name].save_to_db(postgres, ident)
