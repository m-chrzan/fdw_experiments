# Eksperymenty z FDW

Chcemy zbadać wydajność prostych zapytań na dużych ilościach danych, oryginalnie
pochodzących z pliku CSV. Rozważamy następujące podejścia:

* Czytanie prosto z CSVki za pomocą `file_fdw`
* Wprowadzenie danych do jednej zwykłej tabeli
* Wprowadzenie danych do tabeli zapisanej "kolumnowo" dzięki `cstore_fdw`

## Postać danych i zapytania

Rozważamy tabele postaci

    date       | category | v0  | v1  | v2  | ...
    2011-01-01 | A        | 102 | 134 | 95  | ...
    2011-01-01 | B        | 113 | 97  | 95  | ...
    2011-01-02 | A        | 100 | 89  | 93  | ...
    2011-01-02 | B        | 74  | 103 | 120 | ...
    ...

I zapytania postaci:

* suma po `v{i}` w przedziale dat:

    SELECT SUM(v7) FROM table WHERE date BETWEEN <dzień 1> AND <dzień 2>

* jak poprzednio, ale tylko po wierszach z konkretną kategorią:

    ... AND category = '<kategoria>'

## Parametry

* Metoda
    * `file_fdw`
    * zwykła tabela
    * zwykła tabela z indeksem na datach
    * zwykła tabela z indeksem na kategoriach
    * `cstore_fdw`
* Liczba kolumn: 1, *10*, 100, 500
* Przedziały dat w zapytaniach: 1 dzień, 1 tydzień, *1 miesiąc*, 1 rok
* Liczba dni w danych: x1 przedziału dat, x3, x5, dwa lata
* Liczba kategorii: 1, *10*, 100

Za każdym razem badamy wszystie metody. Wyróżnione wartości oznaczają domyślną
wartość parametru.

## Zbierane dane

Dla każdego eksperymentu chcemy zebrać informacje o

* Czasie wykonania zapytań (średnia z trzech odpaleń)
    * Czasami (ale nie zawsze) pierwsze kilka odpaleń (zwykle jedno lub dwa)
      danej metody jest znacznie wolniejsze od reszty. Żeby uniknąć takich
      statystycznych śmieci, odpalamy pięć razy i bierzemy średnią z
      najszybszych trzech.
* Rozmiarze danych na dysku
