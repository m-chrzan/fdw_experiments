import argparse
import csv
import datetime
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('categories', type=int)
parser.add_argument('values', type=int)
parser.add_argument('days', type=int)
parser.add_argument('--mean', type=int, default=100)
parser.add_argument('--deviation', type=float, default=10)
parser.add_argument('--filename', default='gen/output.csv')

args = parser.parse_args()

columns = ['date', 'category'] + ['v{}'.format(i) for i in range(args.values)]
categories = ['c{}'.format(i) for i in range(args.categories)]

one_day = datetime.timedelta(days=1)
with open(args.filename, 'w') as output:
    writer = csv.writer(output, columns)
    writer.writerow(columns)

    date = datetime.date(2015, 1, 1)
    days_written = 0
    while days_written < args.days:
        for category in categories:
            values = [
                '{:d}'.format(int(np.random.normal(args.mean, args.deviation)))
                    for _ in range(args.values)
            ]
            row = [date.isoformat(), category] + values
            writer.writerow(row)

        days_written += 1
        date += one_day
