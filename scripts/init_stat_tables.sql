CREATE TABLE experiment_params (
    exp_id INT,
    categories INT,
    values INT,
    query_days INT,
    total_days INT,
    PRIMARY KEY (exp_id)
);

CREATE TABLE experiment_time (
    exp_id INT,
    time NUMERIC,
    method VARCHAR,
    with_category BOOLEAN
);

CREATE TABLE experiment_size (
    exp_id INT,
    size INT,
    method VARCHAR
);
